import jss from 'jss';
import preset from 'jss-preset-default';
import React, { createContext, useContext, useEffect, useState } from 'react';
import DisconnectWallet, { DisconnectWalletType } from './disconnect-wallet';
import { Ethereum, ethInstance as ethereumConnection } from './ethereum';
import { WalletProvider } from './types';

jss.setup(preset());
const styles = {
  backdrop: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    background: 'rgba(0,0,0,0.2)',
    zIndex: 999
  },
  modal: {
    background: '#ffffff',
    borderRadius: '10px',
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    zIndex: 1000,
    maxWidth: '100%'
  },
  listButton: {
    borderRadius: '20px',
    backgroundColor: '#ffffff',
    border: '1px solid #d1d5da',
    margin: '5px 0',
    padding: '7px 100px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    '&:hover': {
      border: '1px solid #000000',
      cursor: 'pointer'
    }
  },
  header: {
    color: '#000000',
    marginBottom: 15,
    padding: '0 45px 15px',
    borderBottom: '1px solid #d1d5da'
  },
  icon: {
    width: 28,
    height: 28,
    verticalAlign: 'bottom',
    marginRight: '5px'
  },
  button: {
    border: '1px solid #45b8ff',
    fontSize: 16,
    fontWeight: 600,
    color: '#45b8ff',
    fontFamily: 'Work Sans',
    width: 170,
    backgroundColor: 'transparent',
    borderRadius: 0,
    padding: '0 24px',
    height: 46,
    '&:hover': {
      color: '#ffffff',
      borderColor: '#ffffff',
      cursor: 'pointer'
    }
  },
  addressContainer: {
    display: 'flex',
    alignItems: 'center',
    fontFamily: 'Work Sans',
    fontSize: 16,
    fontWeight: 500,
    color: '#efefef'
  },
  walletIcon: {
    width: 18,
    height: 18,
    marginRight: 7,
    backgroundImage: 'radial-gradient(circle at 50% 50%, var(--white), #ff931d 71%)'
  },
  providersListContainer: {
    display: 'flex',
    flexDirection: 'column',
    padding: '0 45px 30px'
  },
  closeButton: {
    position: 'absolute',
    top: '22px',
    right: '20px',
    fontSize: '25px',
    '&:before': {
      content: '"x"'
    },
    '&:hover': {
      cursor: 'pointer'
    }
  }
};
const { classes } = jss.createStyleSheet(styles).attach();

interface ConnectWalletType {
  className?: string;
  listButtonClassName?: string;
  containerClassName?: string;
  headerClassName?: string;
  iconClassName?: string;
  backdropClassName?: string;
  walletIconClassName?: string;
  addressContainerClassName?: string;
  providersListContainerClassName?: string;
  customProviders?: number[];
  onConnect(): void;
  ConnectButtonComponent?: React.FC<{}>;
  WalletAddressComponent?: React.FC<{ currentAccount: string }>;
  logoutIcon?: string;
  providersHeaderText?: string;
  closeButtonClassName?: string;
  showCloseButton?: boolean;
}

interface Provider {
  id: number;
  logo: string;
  name: string;
}

const ConnectWalletContext = createContext<{
  ethereumConnection?: Ethereum;
  closeModal?(): void;
  onConnect?(): void;
}>({});

const Backdrop: React.FC<{ onClick?(): void; className?: string }> = ({ onClick, className }) => (
  <div onClick={onClick} className={className || classes.backdrop}></div>
);

const Modal: React.FC<{ children?: React.ReactNode; className?: string }> = ({
  children,
  className
}) => {
  return <div className={className}>{children}</div>;
};

const ProvidersList: React.FC<{
  providersListContainerClassName: string;
  listButtonClassName: string;
  iconClassName: string;
  customProviders?: number[];
}> = ({ listButtonClassName, iconClassName, customProviders, providersListContainerClassName }) => {
  const ctx = useContext(ConnectWalletContext);

  const providers = ctx.ethereumConnection?.getWalletProviders(customProviders);
  if (providers?.length === 1) {
    ctx.ethereumConnection?.setWalletProvider(providers[0]);
    ctx.closeModal?.();
  }

  return (
    <div className={providersListContainerClassName}>
      {providers?.map((provider) => (
        <ProviderButton
          key={provider.id}
          provider={provider}
          className={listButtonClassName}
          iconClassName={iconClassName}
        />
      ))}
    </div>
  );
};

type ProviderButtonPropsType = {
  provider: Provider;
  className: string;
  iconClassName: string;
};

const ProviderButton: React.FC<ProviderButtonPropsType> = ({
  provider,
  className,
  iconClassName
}) => {
  const ctx = useContext(ConnectWalletContext);

  const handleClick = async (provider: WalletProvider) => {
    await ctx.ethereumConnection?.setWalletProvider(provider);
    ctx.closeModal?.();
    ctx.onConnect?.();
  };

  return (
    <button
      type="button"
      key={provider.id}
      className={className}
      onClick={() => handleClick(provider)}>
      {provider.logo ? <img className={iconClassName} src={provider.logo} /> : null}
      <span>{provider.name}</span>
    </button>
  );
};

const shortenWalletAddress = (address: string) => {
  return address ? `${address.substring(0, 6)}...${address.substring(address.length - 4)}` : '';
};

const DefaultButtonComponent: React.FC<{ onClick(): void; className?: string }> = ({
  onClick,
  className
}) => {
  return (
    <button onClick={onClick} className={className}>
      connect wallet
    </button>
  );
};

const DefaultWalletAddressComponent: React.FC<{
  walletIconClassName?: string;
  currentAccount: string;
  DisconnectWalletComponent: React.FC<DisconnectWalletType>;
  icon?: string;
}> = ({ walletIconClassName, currentAccount, DisconnectWalletComponent, icon }) => {
  return (
    <>
      <div className={walletIconClassName}></div>
      {shortenWalletAddress(currentAccount)}
      <DisconnectWalletComponent icon={icon} />
    </>
  );
};

const ConnectWalletButton: React.FC<{
  onClick(): void;
  className?: string;
  walletIconClassName?: string;
  addressContainerClassName?: string;
  DisconnectWalletComponent: React.FC<DisconnectWalletType>;
  ConnectButtonComponent?: React.FC<{}>;
  WalletAddressComponent?: React.FC<{ currentAccount: string }>;
  logoutIcon?: string;
}> = ({
  onClick,
  className,
  walletIconClassName,
  addressContainerClassName,
  DisconnectWalletComponent,
  ConnectButtonComponent = DefaultButtonComponent,
  WalletAddressComponent = DefaultWalletAddressComponent,
  logoutIcon
}) => {
  const ctx = useContext(ConnectWalletContext);

  return (
    <>
      {ctx.ethereumConnection?.currentAccount ? (
        <div className={addressContainerClassName}>
          <WalletAddressComponent
            walletIconClassName={walletIconClassName}
            currentAccount={ctx.ethereumConnection.currentAccount}
            DisconnectWalletComponent={DisconnectWalletComponent}
            icon={logoutIcon}
          />
        </div>
      ) : (
        <ConnectButtonComponent onClick={onClick} className={className} />
      )}
    </>
  );
};

const ConnectWallet: React.FC<ConnectWalletType> = ({
  className,
  listButtonClassName,
  containerClassName,
  headerClassName,
  iconClassName,
  backdropClassName,
  walletIconClassName,
  addressContainerClassName,
  providersListContainerClassName,
  providersHeaderText = 'Connect wallet',
  closeButtonClassName,
  customProviders,
  onConnect = () => {},
  ConnectButtonComponent,
  WalletAddressComponent,
  logoutIcon,
  showCloseButton = false
}) => {
  const [show, setShow] = useState<boolean>(false);

  const closeModal = () => setShow(false);

  const ContextState = {
    ethereumConnection,
    closeModal,
    onConnect
  };

  useEffect(() => {
    document.addEventListener('account_changed', onConnect);
    return () => {
      document.removeEventListener('account_changed', onConnect);
    };
  }, []);

  return (
    <ConnectWalletContext.Provider value={ContextState}>
      {show && (
        <>
          <Backdrop
            onClick={() => closeModal()}
            className={backdropClassName || classes.backdrop}
          />
          <Modal className={containerClassName || classes.modal}>
            {showCloseButton ? (
              <div
                onClick={closeModal}
                className={closeButtonClassName || classes.closeButton}></div>
            ) : null}

            <h3 className={headerClassName || classes.header}>{providersHeaderText}</h3>
            <ProvidersList
              providersListContainerClassName={
                providersListContainerClassName || classes.providersListContainer
              }
              listButtonClassName={listButtonClassName || classes.listButton}
              iconClassName={iconClassName || classes.icon}
              customProviders={customProviders}
            />
          </Modal>
        </>
      )}
      <ConnectWalletButton
        className={className || classes.button}
        walletIconClassName={walletIconClassName || classes.walletIcon}
        addressContainerClassName={addressContainerClassName || classes.addressContainer}
        logoutIcon={logoutIcon}
        DisconnectWalletComponent={DisconnectWallet}
        onClick={() => setShow(true)}
        ConnectButtonComponent={ConnectButtonComponent}
        WalletAddressComponent={WalletAddressComponent}
      />
    </ConnectWalletContext.Provider>
  );
};

export default ConnectWallet;
