import Papa from 'papaparse';
import React from 'react';
import { EthersErrorMessage } from './types';

type Headers = Record<
  string,
  {
    index: number;
    validate: (value: string, friendlyValues: boolean) => boolean;
  }
>;

const isEthersErrorMessage = (err: any): err is EthersErrorMessage => {
  return typeof err.message === 'string';
};

interface Elements {
  name: string;
  headers: Headers;
}

const ELEMENTS: Elements[] = [];

const ERRORS = {
  WRONG_FORMAT: 'Wrong file format'
};

export const setElements = (elementsToAdd: Elements[]): void => {
  for (const element of elementsToAdd) {
    const exist = ELEMENTS.find((elem) => elem.name === element.name);

    if (!exist) {
      ELEMENTS.push(element);
    }
  }
};

const splitCSVRows = (data: string): string[] => {
  return data.split(/\r\n|\n/);
};

const checkDuplicated = (headers: string[]): boolean => {
  return new Set(headers).size !== headers.length;
};

const validateFileHeaders = (fileHeaders: Headers, reqHeaders: Headers): string => {
  const values = Object.keys(fileHeaders).map((key) => fileHeaders[key]);

  const missingHeader = values.some((header) => header.index < 0 || header.index === undefined);

  if (missingHeader) {
    return `Wrong or missing headers.\nPlease add valid CSV headers line:\n${Object.keys(
      reqHeaders
    ).join(',')}`;
  }

  const isDuplicated = checkDuplicated(Object.keys(fileHeaders));

  if (isDuplicated) {
    return 'Duplicated headers';
  }

  return '';
};

const getFileHeaders = (fileHeaders: string[], reqHeaders: Headers): Headers => {
  const result: Headers = {};

  for (const header of fileHeaders) {
    const index = reqHeaders[header]?.index;
    result[header] = {
      ...reqHeaders[header],
      index
    };
  }

  return result;
};

const getRequiredHeaders = ({
  elementIndex,
  name
}: {
  elementIndex?: number;
  name?: string;
}): Headers => {
  let reqHeaders: Headers = {};

  if (elementIndex) {
    reqHeaders = ELEMENTS[elementIndex].headers;
  } else if (name) {
    const _headers = ELEMENTS.find((elem) => elem.name === name)?.headers;
    if (_headers) reqHeaders = _headers;
  }

  return reqHeaders;
};

const destructCSVLine = (data: string, formType: string): { resultObj: Record<string, string> } => {
  const row = data.replace(/ /g, '').split(',');
  const resultObj: Record<string, string> = {};
  const headers = getRequiredHeaders({ name: formType });

  for (const header in headers) {
    const { index } = headers[header];
    const value = row[index];
    resultObj[header] = value;
  }

  return { resultObj };
};

export const parseSimpleToCSV = (
  data: Record<string, string>[],
  formType: string,
  merkleHeaders?: Headers,
  start: number = 0
): string => {
  let preparedCSVData = '';
  let _merkleHeaders = merkleHeaders;

  if (formType) _merkleHeaders = getRequiredHeaders({ name: formType });
  if (!_merkleHeaders) throw 'Headers not included::parseSimpleToCSV';
  const lastKey = Object.keys(_merkleHeaders).pop();

  for (let i = start; i < data.length; i++) {
    let prep = '';

    for (const header in _merkleHeaders) {
      const isLasKey = header === lastKey;
      const value = data[i][header];
      prep += `${value}${isLasKey ? '' : ','}`;
    }

    preparedCSVData += prep;
    preparedCSVData += i === data.length - 1 ? '' : `\n`;
  }
  return preparedCSVData;
};

export const parseSimpleToJSON = (data: Headers[]) => {
  return JSON.stringify(
    data.map((element) => {
      const cp = { ...element };
      delete cp.index;
      return cp;
    })
  );
};

export const parseAdvancedToSimple = (
  value: string,
  inputType: string,
  headers: boolean,
  formType: string,
  withIndexes: boolean = true
) => {
  const data = [];

  try {
    if (inputType === 'CSV') {
      //from advanced csv to simple
      const lines = splitCSVRows(value);
      const start = headers ? 1 : 0;

      for (let i = start; i < lines.length; i++) {
        const { resultObj } = destructCSVLine(lines[i], formType);

        data.push(resultObj);
      }
    } else {
      // from advanced json array to simple
      const jsonObj: Record<string, string>[] = JSON.parse(value);
      const reqHeaders = getRequiredHeaders({ name: formType });

      for (let i = 0; i < jsonObj.length; i++) {
        const rowData: Record<string, string> = jsonObj[i];
        const resultRowObject: Record<string, string> = {};

        for (const header in reqHeaders) {
          const value = rowData[header];
          resultRowObject[header] = value;
        }

        data.push(resultRowObject);
      }
    }
  } catch (error) {
    console.log('Error while parsing advance to simple values', error);
    return { data };
  }

  return {
    data: withIndexes ? data.map((x, index) => ({ ...x, index })) : data
  };
};

export const parseFile = (
  target: React.ChangeEvent<HTMLInputElement>['target'],
  formType: string,
  inputType: string,
  callback: (arg0: Record<string, string | Record<string, string>[] | undefined>) => void,
  includeHeaders: boolean = false,
  csvResponse: boolean = true
) => {
  const isCSVMode = inputType === 'CSV';
  const isJSONFile = target.files?.[0].type === 'application/json';
  const isCSVFile = target.files?.[0]?.type === 'text/csv';

  if (isCSVMode && isJSONFile) {
    throw new Error(`${ERRORS.WRONG_FORMAT}. Expected CSV file.`);
  }

  if (!isCSVMode && isCSVFile) {
    throw new Error(`${ERRORS.WRONG_FORMAT}. Expected JSON file.`);
  }

  if (!isCSVFile && !isJSONFile) {
    throw new Error(`Unsupported file format. Only JSON or CSV.`);
  }

  if (isJSONFile) {
    const fileReader = new FileReader();
    fileReader.readAsText(target.files?.[0] as File, 'UTF-8');
    fileReader.onload = (e: ProgressEvent<FileReader>) => {
      try {
        const target = e.target;
        if (!target) throw 'Target undefined::parseFiles';
        const stringValue = target.result?.toString() ?? '';
        const reqHeadersData = getRequiredHeaders({ name: formType });
        const reqHeaders = Object.keys(reqHeadersData);
        const res = JSON.parse(stringValue);

        for (const leaf of res) {
          const hasReqKeys = reqHeaders.every((key) => key in leaf);
          if (!hasReqKeys) {
            throw new Error(`Wrong JSON object keys.\nShould be: ${reqHeaders.join(',')}`);
          }
        }

        callback({ parsedData: stringValue });
      } catch (_error: EthersErrorMessage | unknown) {
        if (isEthersErrorMessage(_error)) {
          callback({ error: _error?.message });
        }
      }
    };
  }

  if (isCSVFile) {
    Papa.parse(target.files?.[0] as File, {
      skipEmptyLines: true,
      complete: function (results) {
        let error;
        let parsedData;

        try {
          parsedData = parseFileData(
            results.data as string[][],
            formType,
            includeHeaders,
            csvResponse
          );
        } catch (_error: EthersErrorMessage | unknown) {
          console.log('Error while parsing CSV file.', _error);
          if (isEthersErrorMessage(_error)) {
            error = _error?.message;
          }
        } finally {
          callback({ parsedData, error });
        }
      }
    });
  }
};

const parseFileData = (
  leaves: Array<string[]>,
  formType: string,
  includeHeaders: boolean,
  csvResponse: boolean = true
) => {
  let headers: Headers = {};
  let error;

  if (includeHeaders) {
    const reqHeaders = getRequiredHeaders({ name: formType });
    headers = getFileHeaders(leaves[0], reqHeaders);
    error = validateFileHeaders(headers, reqHeaders);

    if (error) throw new Error(error);

    leaves.shift();
  } else {
    headers = getRequiredHeaders({ name: formType });
  }

  const preparedData = [];
  for (const leaf of leaves) {
    const parsedElement: Record<string, string> = {};

    Object.keys(headers).map((header) => {
      const { index } = headers[header];
      const leafValue = leaf[index];
      if (!leaf[index]) {
        throw new Error('Wrong number of columns in file');
      } else {
        parsedElement[header] = leafValue;
      }
    });

    preparedData.push(parsedElement);
  }

  const parsedData = csvResponse ? parseSimpleToCSV(preparedData, '', headers) : preparedData;

  return parsedData;
};

export const parseCsvToArray = (csv: string): Array<string[]> => {
  const rows = splitCSVRows(csv);
  return rows.map((row) => row.split(','));
};

export const parseJSONToCSV = (value: string, formType: string) => {
  const data = JSON.parse(value);
  const preparedCSVData = parseSimpleToCSV(data, formType); // json array object the same as simple mode
  return preparedCSVData;
};

export const parseCSVToJSON = (
  value: string,
  formType: string,
  includeHeaders: boolean = false
) => {
  const lines = splitCSVRows(value);

  const start = includeHeaders ? 1 : 0;
  let preparedJSONData = '[';

  for (let i = start; i < lines.length; i++) {
    const { resultObj } = destructCSVLine(lines[i], formType);
    preparedJSONData += `${JSON.stringify(resultObj)}${i === lines.length - 1 ? '' : `,\n`}`;
  }

  preparedJSONData += ']';

  return { preparedJSONData };
};
