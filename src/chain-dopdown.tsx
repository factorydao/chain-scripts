import React, { createContext, useContext, useEffect, useState } from 'react';
import Select, { SingleValue } from 'react-select';
import bscIcon from './assets/bsc.png';
import ethIcon from './assets/ethereum.png';
import optimismIcon from './assets/optimism.png';
import polIcon from './assets/polygon.png';
import networks from './configs/networks';
import {
  defaultChainId,
  Ethereum,
  ethInstance as ethereumConnection,
  LOCAL_STORAGE
} from './ethereum';

type Option = {
  value: string;
  label?: JSX.Element;
};

type optionClasses = {
  optionClassName?: string;
  optionIconClassName?: string;
  optionLabelClassName?: string;
};

type ChainIcons = Record<number, string>;

const chainIcons: ChainIcons = {
  1: ethIcon.src || ethIcon,
  10: optimismIcon.src || optimismIcon,
  56: bscIcon.src || bscIcon,
  137: polIcon.src || polIcon
};

const generateOptions = (
  supportedChains: Array<number>,
  shortName: boolean = false,
  optionClasses: optionClasses
) => {
  const _defaultChainId = defaultChainId.toString();
  let options: Option[] = [];

  const generateLabel = (
    chainData: {
      name: string;
      shortName?: string;
    },
    chainIcon: string,
    { optionClassName, optionIconClassName, optionLabelClassName }: optionClasses
  ) => {
    return (
      <div className={optionClassName}>
        <div className={optionIconClassName}>{chainIcon && <img src={chainIcon} />}</div>
        <div className={optionLabelClassName}>
          {shortName && chainData.shortName ? chainData.shortName : chainData.name}
        </div>
      </div>
    );
  };

  if (supportedChains.length === 0) {
    const chainData = networks[_defaultChainId];
    const chainIcon = chainIcons[parseInt(_defaultChainId) as keyof ChainIcons];
    options = [
      {
        value: _defaultChainId,
        label: generateLabel(chainData, chainIcon, optionClasses)
      }
    ];
  }

  supportedChains.map((chain) => {
    const chainData = networks[chain];
    const chainIcon = chainIcons[chain];

    if (chainData) {
      options.push({
        value: chain.toString(),
        label: generateLabel(chainData, chainIcon, optionClasses)
      });
    }
  });
  return options;
};

interface ChainDropdown {
  className?: string;
  onChange?(chain: string): void;
  optionClassName?: string;
  optionIconClassName?: string;
  optionLabelClassName?: string;
  shortName?: boolean;
  supportedChains?: Array<number>;
  hideOneOption?: boolean;
  styles?: Record<string, string>;
}

const ChainDropdownContext = createContext<{
  ethereumConnection?: Ethereum;
}>({});

const ChainDropdownButton: React.FC<ChainDropdown> = ({
  className,
  onChange = (chain) => {},
  optionClassName,
  optionIconClassName,
  optionLabelClassName,
  shortName = false,
  supportedChains = [],
  hideOneOption = true,
  styles
}) => {
  
  const ctx = useContext(ChainDropdownContext);

  const options = generateOptions(supportedChains, shortName, {
    optionClassName,
    optionIconClassName,
    optionLabelClassName
  });

  const [selectedValue, setSelectedValue] = useState(options[0]);
  const [currentChain, setCurrentChain] = useState<Number>(parseInt(selectedValue.value));

  const disableOptions = options.length <= 1 && hideOneOption;

  const chainOnChange: (value: SingleValue<Option>) => void = (option) => {
    if (!option) return;
    const newChain = parseInt(option.value);
    if (currentChain === newChain) return;

    console.warn(`Detected network change from ${currentChain} to ${newChain}`);

    localStorage.setItem(LOCAL_STORAGE.CURRENT_CHAIN, newChain.toString());
    ctx.ethereumConnection?.handleChainChanged(newChain);

    setCurrentChain(newChain);
    const selectedValue = options.find((x) => x.value === newChain.toString());
    setSelectedValue(selectedValue ? selectedValue : options[0]);

    onChange(option.value);
  };

  useEffect(() => {
    if (supportedChains.length === 0) return;

    const localChainId = localStorage.getItem(LOCAL_STORAGE.CURRENT_CHAIN);
    let chainToSet = localChainId;

    if (!options.some((x) => x.value === localChainId)) chainToSet = options[0].value;
    if (!chainToSet) return;
    localStorage.setItem(LOCAL_STORAGE.CURRENT_CHAIN, chainToSet);
    if (ctx.ethereumConnection) {
      ctx.ethereumConnection.currentChainId = parseInt(chainToSet);
    }
    const selectedValue = options?.find((x) => x.value === chainToSet);
    setSelectedValue(selectedValue ?? options[0]);
    setCurrentChain(parseInt(chainToSet));
    onChange(chainToSet);
  }, [supportedChains]);

  return (
    <>
      <Select
        className={className}
        defaultValue={options[0]}
        isSearchable={false}
        options={options}
        styles={styles}
        onChange={chainOnChange}
        isDisabled={disableOptions}
        value={selectedValue}
      />
    </>
  );
};

const ChainDropdown: React.FC<ChainDropdown> = ({
  className,
  onChange,
  optionClassName,
  optionIconClassName,
  optionLabelClassName,
  supportedChains,
  shortName,
  hideOneOption,
  styles
}) => {
  const ContextState = {
    ethereumConnection
  };

  return (
    <ChainDropdownContext.Provider value={ContextState}>
      <ChainDropdownButton
        supportedChains={supportedChains}
        onChange={onChange}
        className={className}
        optionClassName={optionClassName}
        optionIconClassName={optionIconClassName}
        optionLabelClassName={optionLabelClassName}
        shortName={shortName}
        hideOneOption={hideOneOption}
        styles={styles}
      />
    </ChainDropdownContext.Provider>
  );
};

export default ChainDropdown;
